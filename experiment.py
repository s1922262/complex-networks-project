import networkx as nx
import numpy as np
import random
import matplotlib.pyplot as plt
from datetime import datetime
import pickle
import signal
import time
from func_timeout import func_timeout, FunctionTimedOut


from graph import *
from recover import *
from rewire import *
import pathlib

TARGET_ATTACK = 'target'
RANDOM_ATTACK = 'random'
SIZE = 250
PORB_EDGE = 0.1 # for ER random graph
SF_type = 'SF'
ER_type = 'ER'
TIME_OUT = 60*60

GRAPHS = [SF_type]
ATTACKS = [TARGET_ATTACK, RANDOM_ATTACK]
ATTACK_LEVEL = [0.5] # Fraction of edges to be removed
recovery_option = [20, 21, 22, 200]


def experiment_d_lcc(graph_type=SF_type, attack_type=TARGET_ATTACK, size=SIZE, attack_level=ATTACK_LEVEL,
                     recovery_option=recovery_option, prob_edge=PORB_EDGE):

    G = graph(graph_type, size, prob_edge)
    initial_diameter, initial_lcc, av = get_robustness(G)
    d_lcc_first_attack = []
    path = str(pathlib.Path().absolute()) + "/Experiments/" + graph_type + "/" + attack_type + "/size" + str(size) + "_"
    edges_removed_during_attack = np.array(attack_level) * G.size()

    num_edges = {}
    d_lcc_second_attack = {}
    for ro in recovery_option:
        d_lcc_second_attack[ro] = []
        num_edges[ro] = []

    for al in attack_level:
        # First attack
        G_attack_1 = targeted_edge_attack(G, al)
        d, lcc, av = get_robustness(G_attack_1)
        d_lcc_first_attack.append(d / lcc)

        for ro in recovery_option:
            # Recovery
            G_recovered, ne = recover_to_initial_diameter_lcc_ratio(initial_diameter, initial_lcc, G_attack_1,
                                                                    recovery_option=ro)
            num_edges[ro].append(ne)

            # Second attack
            G_attack_2 = targeted_edge_attack(G_recovered, al)
            d2, lcc2, av = get_robustness(G_attack_2)
            d_lcc_second_attack[ro].append(d2 / lcc2)

        print("Finished attack level", al)

    # Save data
    data = {'attack_level': attack_level,
            'd_lcc_first_attack': d_lcc_first_attack,
            'd_lcc_second_attack': d_lcc_second_attack,
            'recovery_option': recovery_option,
            'edges_removed_during_attack': edges_removed_during_attack,
            'num_edges': num_edges}
    with open(path+'all_data.pickle', 'wb') as handle:
        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)

    # Make plots
    plot_d_lcc_add(data['attack_level'], data['d_lcc_first_attack'], data['d_lcc_second_attack'], data['recovery_option'], path)
    plot_num_edges_add(attack_level, edges_removed_during_attack, recovery_option, num_edges, path)
    plot_d_lcc_rewire(attack_level, d_lcc_second_attack, recovery_option, path)
    plot_num_edges_rew(attack_level, edges_removed_during_attack, recovery_option, num_edges, path)


def experiment_constant_edges(graph_type=SF_type, attack_type=TARGET_ATTACK, size=SIZE, attack_level=ATTACK_LEVEL,
                     recovery_option=recovery_option, prob_edge=PORB_EDGE):

    G = graph(graph_type, size, prob_edge)

    initial_diameter, initial_lcc, av = get_robustness(G)
    d_lcc_first_attack = []
    path = str(pathlib.Path().absolute()) + "/Experiments/" + graph_type + "/" + attack_type + "/edges/EDGE_size" + str(size) + "_"
    edges_removed_during_attack = np.array(attack_level) * G.size()

    num_edges = {}
    d_lcc_second_attack = {}
    for ro in recovery_option:
        d_lcc_second_attack[ro] = []
        num_edges[ro] = []

    index = 0
    for al in attack_level:
        # First attack
        G_attack_1 = targeted_edge_attack(G, al)
        d, lcc, av = get_robustness(G_attack_1)
        d_lcc_first_attack.append(d / lcc)

        initial_edges = edges_removed_during_attack[index]

        for ro in recovery_option:
            # Recovery
            G_recovered, ne = recover_to_same_edges(initial_edges=initial_edges, attacked_graph=G_attack_1,
                                                                    recovery_option=ro)
            num_edges[ro].append(ne)

            # Second attack
            G_attack_2 = targeted_edge_attack(G_recovered, al)
            d2, lcc2, av = get_robustness(G_attack_2)
            d_lcc_second_attack[ro].append(d2 / lcc2)

        index+=1
        print("Finished attack level", al)

    # Save data
    data = {'attack_level': attack_level,
            'd_lcc_first_attack': d_lcc_first_attack,
            'd_lcc_second_attack': d_lcc_second_attack,
            'recovery_option': recovery_option,
            'edges_removed_during_attack': edges_removed_during_attack,
            'num_edges': num_edges}
    with open(path+'all_data.pickle', 'wb') as handle:
        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)

    # Make plots
    plot_d_lcc_add(data['attack_level'], data['d_lcc_first_attack'], data['d_lcc_second_attack'], data['recovery_option'], path)
    plot_num_edges_add(attack_level, edges_removed_during_attack, recovery_option, num_edges, path)
    plot_d_lcc_rewire(attack_level, d_lcc_second_attack, recovery_option, path)
    plot_num_edges_rew(attack_level, edges_removed_during_attack, recovery_option, num_edges, path)

if __name__ == "__main__":
    for G_type_index in range(len(GRAPHS)):
        G_type = GRAPHS[G_type_index]
        print("\n")
        attack_type_index = 0
        while attack_type_index < len(ATTACKS):
            attack_type = ATTACKS[attack_type_index]
            print("Graph: ", G_type, "\nAttack type: ", attack_type)
            try:
                func_timeout(TIME_OUT, experiment_constant_edges, args=(G_type, attack_type))
                print("Successfully finished {} attack for {} graph".format(attack_type, G_type))
                attack_type_index += 1
            except FunctionTimedOut:
                print("The graph with size {} takes too long. Reducing size.".format(SIZE))
                SIZE = SIZE/2
        G_type_index += 1
        # experiment_d_lcc(graph_type=G_type, attack_type=attack_type, size=size)
