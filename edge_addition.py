import networkx as nx
import numpy as np
import random
import matplotlib.pyplot as plt

from robustness import *
from attack import *
import recover
from data import *

RANDOM_ADD = 10  # Add a random edge
PREFERENTIAL_MIN_MIN = 11    # Connect 2 nodes with the smallest degrees
PREFERENTIAL_MIN_MAX = 12    # Connect nodes with the smallest and largest degrees
PREFERENTIAL_MAX_MAX = 13    # Connect 2 nodes with the largest degrees
PREFERENTIAL_MAX_RAND = 14    # Connect a node with the largest degree to a random node
NO_ADD = 100

def random_addition(graph, fr):
    if fr > 1 or fr < 0:
        raise Exception("Fraction of edges to add has to be between 0 and 1.")
    graph = graph.copy()
    # size of graph is the number of edges
    size = graph.size()
    # number of edges to be added
    n = size * fr
    for i in range(int(n)):
        graph = add_random_edge(graph)
    return graph


def preferential_addition(graph, fr):
    if fr > 1 or fr < 0:
        raise Exception("Fraction of edges to add has to be between 0 and 1.")
    graph = graph.copy()
    # size of graph is the number of edges
    size = graph.size()
    # number of edges to be added
    n = size * fr
    for i in range(int(n)):
        graph = add_preferential_edge_min_min(graph)
    return graph


def add_random_edge(graph):
    """
    Create a new random edge.
    :param graph: networkx graph
    :return: networkx graph
    """
    nonedges = list(nx.non_edges(graph))

    if nonedges:
        chosen_nonedge = random.choice(nonedges)
        graph.add_edge(chosen_nonedge[0], chosen_nonedge[1])

    return graph


def add_preferential_edge_min_min(graph):
    """
    Add an edge between 2 vertices with the least degree.
    :param graph: networkx graph
    :return: networkx graph
    """
    graph = graph.copy()
    nodes = sorted(graph.degree, key=lambda x: x[1], reverse=False)
    n1 = nodes[0]
    n2 = nodes[1]
    i = 1
    while graph.has_edge(n1, n2):
        i += 1
        n2 = nodes[i]
    graph.add_edge(n1, n2)
    return graph


def add_preferential_edge_min_max(graph):
    """
    Add an edge between 2 vertices with the smallest and largest degree.
    :param graph: networkx graph
    :return: networkx graph
    """
    graph = graph.copy()
    nodes = sorted(graph.degree, key=lambda x: x[1], reverse=False)
    n_small = nodes[0][0]
    n_large = nodes[-1][0]
    i = 1
    while graph.has_edge(n_small, n_large):
        n_large = nodes[-1 - i][0]
        i += 1
    graph.add_edge(n_small, n_large)
    return graph


def add_preferential_edge_max_rand(graph):
    """
    Add an edge between 2 vertices with the smallest and largest degree.
    :param graph: networkx graph
    :return: networkx graph
    """
    graph = graph.copy()
    nodes = sorted(graph.degree, key=lambda x: x[1], reverse=True)
    n_random = random.choice(nodes)[0]
    n_large = nodes[0][0]
    while graph.has_edge(n_random, n_large):
        n_random = random.choice(nodes)[0]
    graph.add_edge(n_random, n_large)
    return graph


def add_preferential_edge_max_max(graph):
    """
    Add an edge between 2 vertices with the largest degree.
    :param graph: networkx graph
    :return: networkx graph
    """
    graph = graph.copy()
    nodes = sorted(graph.degree, key=lambda x: x[1], reverse=True)
    i = 1
    n_small = nodes[0][0]
    n_large = nodes[i][0]
    while graph.has_edge(n_small, n_large):
        i = i + 1
        if i >= len(nodes):
            break
        else:
            n_large = nodes[i][0]
    graph.add_edge(n_small, n_large)
    return graph

