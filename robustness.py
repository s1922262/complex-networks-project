import networkx as nx
import numpy as np
import random
import matplotlib.pyplot as plt


def get_diameter(graph):
    # For a disconnected graph graph, its diameter is equal to the maximum value of diameters of its connected
    # components
    d = 0
    for C in (graph.subgraph(c).copy() for c in nx.connected_components(graph)):
        d = max(d, nx.average_shortest_path_length(C))
    #         print("Diameter of a subgraph", nx.average_shortest_path_length(C))
    return d


def get_LCC_size(graph):
    return len(max(nx.connected_components(graph), key=len))


def av_cc_size(graph):
    all_sizes = [len(c) for c in sorted(nx.connected_components(graph), key=len, reverse=False)]
    lcc_size = all_sizes.pop()
    av = np.average(np.array(all_sizes))
    if av == av:  # Check if element is not NaN
        return av
    else:
        return 0


def get_robustness(graph):
    return get_diameter(graph), get_LCC_size(graph), av_cc_size(graph)


def print_robustness(graph):
    d, lcc, av_cc = get_robustness(graph)
    print("Diameter:", d)
    print("Size of Largest Connected Component:", lcc)
    print("Average isolated connected component size:", av_cc)
