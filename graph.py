import networkx as nx
import numpy as np
import random
import matplotlib.pyplot as plt

SIZE = 100
PORB_EDGE = 0.01
SF_type = 'SF'
ER_type = 'ER'


def graph(graph_type=SF_type, size=SIZE, prob_edge=PORB_EDGE):
    if graph_type == SF_type:
        return SF_graph(size)
    else:
        return ER_graph(size, prob_edge)


def ER_graph(size, prob_edge):
    G = nx.erdos_renyi_graph(size, prob_edge)
    G.graph.update({'type': ER_type})
    return G


def SF_graph(size):
    G = nx.scale_free_graph(size).to_undirected()
    G.graph.update({'type': SF_type})
    return G
