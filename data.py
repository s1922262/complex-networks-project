import json
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import make_interp_spline
from scipy.interpolate import make_interp_spline, BSpline
import pickle
import pathlib

RANDOM_ADD = 10  # Add a random edge
PREFERENTIAL_MIN_MIN = 11    # Connect 2 nodes with the smallest degrees
PREFERENTIAL_MIN_MAX = 12    # Connect nodes with the smallest and largest degrees
PREFERENTIAL_MAX_MAX = 13    # Connect 2 nodes with the largest degrees
PREFERENTIAL_MAX_RAND = 14    # Connect a node with the largest degree to a random node
RANDOM_REWIRE = 20  # Remove and add random edge
PREFERENTIAL_REW = 21     # Disconnect a random edge from a highest-degree node, and reconnect that edge to a random node
PREFERENTIAL_RANDOM = 22     # Choose a random edge, disconnect it from its higher-degree node, and reconnect that edge to a random node.
NO_REWIRE = 200
NO_ADD = 100

TARGET_ATTACK = 'target'
RANDOM_ATTACK = 'random'
SIZE = 250
PORB_EDGE = 0.1 # for ER random graph
SF_type = 'SF'
ER_type = 'ER'
TIME_OUT = 60*60

labels = {RANDOM_ADD: "Add_00",
          PREFERENTIAL_MIN_MIN: "Add_11",
          PREFERENTIAL_MAX_MAX: "Add_22",
          PREFERENTIAL_MIN_MAX: "Add_12",
          PREFERENTIAL_MAX_RAND: "Add_20",

          RANDOM_REWIRE: "Rew_00",
          PREFERENTIAL_REW: "Rew_20",
          PREFERENTIAL_RANDOM: "Rew_02",

          NO_ADD: "No_Add",
          NO_REWIRE: "No_Rew"}

colors = {RANDOM_ADD: 'slateblue',
          PREFERENTIAL_MIN_MIN: 'green',
          PREFERENTIAL_MAX_MAX: 'crimson',
          PREFERENTIAL_MIN_MAX: 'orange',
          PREFERENTIAL_MAX_RAND: 'lightpink',

          RANDOM_REWIRE: 'slateblue',
          PREFERENTIAL_REW: 'green',
          PREFERENTIAL_RANDOM: 'orange',

          NO_ADD: "grey",
          NO_REWIRE: 'grey'}


def plot_diameter(attack_level, diameter_first_attack, recovery_option, diameter_second_attack, path):
    plt.plot(attack_level, diameter_first_attack, label="first attack", color='#000000')
    for ro in recovery_option:
        plt.plot(attack_level, diameter_second_attack[ro], label=labels[ro])

    plt.xlabel("Attack level")
    plt.ylabel("Diameter")

    plt.legend()

    plt.savefig(path+"diameter.pdf")
    plt.clf()


def plot_num_edges_add(attack_level, edges_removed_during_attack, recovery_option, num_edges, path):
    # plt.plot(attack_level, edges_removed_during_attack, label="attack", color='r')

    for ro in recovery_option:
        if ro < 20:
            fr_edges_added = np.array(num_edges[ro])/ np.array(edges_removed_during_attack)
            x, y = smooth(attack_level, fr_edges_added)
            plt.plot(x, y, label=labels[ro], color=colors[ro])
            plt.plot(attack_level,  [1]*len(attack_level), 'k--')
    plt.xlabel("Attack level")
    plt.ylabel("#edges added/#edges removed ")

    plt.legend()

    plt.savefig(path + "edges_add.pdf")
    plt.clf()


def plot_num_edges_rew(attack_level, edges_removed_during_attack, recovery_option, num_edges, path):
    i = 0
    for ro in recovery_option:
        if 20 <= ro < 100:
            edges_rewired = num_edges[ro]
            x, y = smooth(attack_level, edges_rewired)
            # plt.plot(x, y, label=labels[ro], color=colors[ro], linewidth=3-i)
            plt.plot(x, y, label=labels[ro], color=colors[ro])
            i+=1
    plt.xlabel("Attack level")
    plt.ylabel("#edges rewired")

    plt.legend()

    plt.savefig(path + "edges_rew.pdf")
    plt.clf()


def plot_d_lcc_rewire(attack_level, d_lcc_second_attack, recovery_option, path, initial_robustness=None):

    x, y = smooth(attack_level, initial_robustness)
    plt.plot(x, y, label="First attack", color = 'k', linestyle='--')

    for ro in recovery_option:
        if ro >= 20 and ro != 100:
            x, y = smooth(attack_level, d_lcc_second_attack[ro])
            if ro == 200:
                plt.plot(x, y, color=colors[ro], linestyle='--',  label=labels[ro])
            else:
                plt.plot(x, y, label=labels[ro], color=colors[ro])

    plt.xlabel("Attack level")
    plt.ylabel("Diameter/LCC")

    plt.legend()

    # plt.show()
    plt.savefig(path + "diameter_lcc_rewire.pdf")
    plt.clf()
    exit()


def plot_d_lcc_add(attack_level, d_lcc_first_attack, d_lcc_second_attack, recovery_option, path):
    plt.plot(attack_level, d_lcc_first_attack, label="First attack", color='k', linestyle='--')
    for ro in recovery_option:
        x, y = smooth(attack_level, d_lcc_second_attack[ro])
        if ro == 100:
            plt.plot(x, y, color=colors[ro], linestyle='--', label=labels[ro])
        if ro < 20:
            plt.plot(x, y, label=labels[ro], color=colors[ro])

    plt.xlabel("Attack level")
    plt.ylabel("Diameter/LCC")

    plt.legend()

    plt.savefig(path + "diameter_lcc_addition.pdf")
    plt.show()
    plt.clf()


def save_attack_level(attack_level):
    f = open("Experiments/attack_level.txt", "x")
    json.dump(attack_level, f)
    f.close()


def save_data(num_edges, diameter_first_attack, diameter_second_attack, path):
    f = open(path + "num_edges.txt", "x")
    json.dump(num_edges, f)
    f.close()

    f = open(path + "diameter_second_attack.txt", "x")
    json.dump(diameter_second_attack, f)
    f.close()

    f = open(path + "diameter_first_attack.txt", "x")
    json.dump(diameter_first_attack, f)
    f.close()


def smooth(x, y):
    xnew = np.linspace(x[0], x[-1], 10)
    spl = make_interp_spline(x, y)
    y_new = spl(xnew)
    return xnew, y_new
    # return x, y


if __name__ == "__main__":
    path = str(pathlib.Path().absolute()) + "/Experiments/" + SF_type + "/" + RANDOM_ATTACK + "/size" + '250' + "_"

    with open(path+'all_data.pickle', 'rb') as handle:
        data = pickle.load(handle)

    plot_d_lcc_rewire(attack_level=data['attack_level'],
                      d_lcc_second_attack=data['d_lcc_second_attack'],
                      recovery_option=data['recovery_option'],
                      path=path,
                      initial_robustness=data['d_lcc_first_attack'])

