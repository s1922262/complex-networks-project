##Login 
```
ssh s1922262@korenvliet.ewi.utwente.nl
```

## File transfer 
Copy directory
```
scp -r "/path/to/sourse" s1922262@korenvliet.ewi.utwente.nl:path/to/destination
```

Copy file 
```
scp /path/to/file s1922262@korenvliet.ewi.utwente.nl:/path/to/destination
```

Transfer all files form source to current local directory
```
scp -r s1922262@korenvliet.ewi.utwente.nl:/path/to/sourse .
```

###SLRUM
Run job 
```
sbatch run.sh
```
Get all my running jobs
```
squeue -u s1922262
```
Get job info
```
sacct -j jobid
```
Cancel job
```
scancel jobid
```

## Useful terminal commands
Get current path
```
pwd
```

Create a new directory
```
mkdir name
```

Show content of the current directory 

```
ls
```
###VIM
```
vi run.sh
```
`CMD+I, :q, :wq`

## Run script
```
#!/bin/bash
#SBATCH --job-name=CN.job
#SBATCH --error=CN.err # where errors are logged
#SBATCH -o CN.out      # log output of the script
#SBATCH -c 2           # number of cores, 2

#SBATCH --mail-type=END     # get email on job end
#SBATCH --mail-user=user@gmail.com

python3 -u run.py
```